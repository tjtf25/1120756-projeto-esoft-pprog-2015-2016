/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AvaliarCandidaturaController;
import Model.Candidatura;
import Model.CentroExposicoes;
import Model.Avaliacao;
import Model.Exposicao;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Tiago Faria
 */
public class AvaliarCandidaturaUI {
    
    private final CentroExposicoes m_centro_exposicoes;
    private final AvaliarCandidaturaController controller;
    
    public AvaliarCandidaturaUI(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
        controller = new AvaliarCandidaturaController(m_centro_exposicoes);
    }
    
    public void run() {
        String id = "";
        List<Exposicao> exposicoes = controller.getExposicaoFAE(id);
        
        String exp;
        
        boolean opt = true;
        
        do {
            // apresentar as exposições
            System.out.println("-- Exposições --");
            for(Exposicao e : exposicoes){
                System.out.println(e.mostraSimples());
            }
       
            exp = Utils.readLineFromConsole("Escolha pelo título (0-terminar):");

            if(!exp.equals("0")) {
                Exposicao e = existeExposicao(exp, exposicoes);
                if (e != null) {
                    controller.selectExposicao(e);
                    opt = false;
                    selecionaCandidatura();
                }
            }
        } while (opt && !exp.equals("0"));
    }
    
    private void selecionaCandidatura() {
        String id = "";
        List<Candidatura> candidaturas = controller.getCandidaturaFAE(id);
        
        String nome;
        
        boolean opt = true;
        
        do {
            // apresentar as candidaturas
            System.out.println("-- Candidaturas --");
            for(Candidatura c : candidaturas){
                System.out.println(c.mostraSimples());
            }
       
            nome = Utils.readLineFromConsole("Escolha pelo nome (0-terminar):");

            if(!nome.equals("0")) {
                Candidatura c = existeCandidatura(nome, candidaturas);
                if (c != null) {
                    controller.selectCandidatura(c);
                    
                    tomarDecisao();
                }
            }
        } while (!nome.equals("0"));
    }
    
    private void tomarDecisao() {
        controller.novaAvaliacao();
        
        Avaliacao d = introduzDadosDecisao();
        
        controller.validaAvaliacao();
        
        System.out.println(d.toString());
        
        if (Utils.confirmacao("Confirma dados (s/n)?").equals("s")) {
            controller.registaAvaliacao();
            System.out.println("A decisao foi registada com sucesso: " + d.toString());
        } else {
            System.out.println("A decisão não foi registada por decisão do utilizador!");
        }
    }
    
    private Avaliacao introduzDadosDecisao() {
        System.out.println("Introdução de dados da decisão");
        boolean decisao;
        do {
            if (Utils.readLineFromConsole("Decisão (s/n): ").equals("s")) {
                decisao = true;
                break;
            } else if (Utils.readLineFromConsole("Decisão (s/n): ").equals("n")) {
                decisao = false;
                break;
            } else {
                System.out.println("Opção inválida!");
            }
        } while (true);
        String justificacao = Utils.readLineFromConsole("Justificação: ");
        return controller.setDados(decisao, justificacao);
    }
    
    private Exposicao existeExposicao(String exp, List<Exposicao> lista) {
        for(Exposicao e : lista) {
            if(e != null && e.getTitulo().equals(exp)) {
                return e;
            }
        }
        return null;
    }
    
    private Candidatura existeCandidatura(String nome, List<Candidatura> lista) {
        for(Candidatura c : lista) {
            if(c != null && c.getEmpresa().equals(nome)) {
                return c;
            }
        }
        return null;
    }
}
