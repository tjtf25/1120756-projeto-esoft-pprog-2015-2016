/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.CentroExposicoes;
import utils.Utils;
import java.io.IOException;

/**
 *
 * @author Tiago Faria
 */
public class MenuUI {
    
    private CentroExposicoes m_centro_exposicoes;
    private String opcao;

    public MenuUI(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
    }

    public void run() throws IOException {
        do {
            System.out.println("1. Criar Exposição");
            System.out.println("2. Definir FAE");
            System.out.println("3. Atribuir Candidatura");
            System.out.println("4. Decidir Candidatura");
            System.out.println("5. Criar Candidatura");
            System.out.println("6. Criar Perfil de Utilizador");
            System.out.println("7. Confirmar Registo de Utilizador");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            switch (opcao) {
                case "1":
                    CriarExposicaoUI uiCE = new CriarExposicaoUI(m_centro_exposicoes);
                    uiCE.run();
                    break;
                case "2":
                    DefinirFAEUI uiDFAE = new DefinirFAEUI(m_centro_exposicoes);
                    uiDFAE.run();
                    break;
                case "3":
                    AtribuirCandidaturaUI uiAC = new AtribuirCandidaturaUI(m_centro_exposicoes);
                    uiAC.run();
                    break;
                case "4":
                    AvaliarCandidaturaUI uiDC = new AvaliarCandidaturaUI(m_centro_exposicoes);
                    uiDC.run();
                    break;
                case "5":
                    CriarCandidaturaUI uiCC = new CriarCandidaturaUI(m_centro_exposicoes);
                    uiCC.run();
                    break;
                case "6":
                    CriarPerfilUtilizadorUI uiCPU = new CriarPerfilUtilizadorUI(m_centro_exposicoes);
                    uiCPU.run();
                    break;
                case "7":
                    ConfirmarUtilizadorUI uiCU = new ConfirmarUtilizadorUI(m_centro_exposicoes);
                    uiCU.run();
                    break;
            }
        } while (!opcao.equals("0") );
    }
}
