/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.ConfirmarUtilizadorController;
import Model.CentroExposicoes;
import Model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Tiago Faria
 */
public class ConfirmarUtilizadorUI {
    
    private final CentroExposicoes m_centro_exposicoes;
    private final ConfirmarUtilizadorController controller;

    public ConfirmarUtilizadorUI(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
        controller = new ConfirmarUtilizadorController(m_centro_exposicoes);
    }
    
    public void run() {
        List<Utilizador> lu = controller.getUtilizadoresNaoConfirmados();
        List<Utilizador> luEscolhidos = new ArrayList<Utilizador>();
        
        String user;
        
        do {
            // apresentar os utilizadores
            System.out.println("-- Utilizadores Não Confirmados --");
            for(Utilizador u : lu){
                if(!existeLista(u,luEscolhidos)) {
                    System.out.println(u.mostraSimples());
                }
            }
       
            user = Utils.readLineFromConsole("Escolha pelo user (0-terminar):");

            if(!user.equals("0")) {
                Utilizador u = existe(user, lu);
                if (u != null) {
                    
                    controller.selectUtilizador(u);
                    
                    System.out.println(u.toString());
                    
                    // solicita confirmação
                    if (Utils.confirmacao("Confirma o Utilizador (s/n)?").equals("s")) {
                        controller.confirmarUtilizador();
                        luEscolhidos.add(u);
                        System.out.println("Utilizador confirmado com sucesso!");
                    } else {
                        System.out.println("O Utilizador não foi confirmado por decisão do utilizador!");
                    }
                }
            }
        } while (!user.equals("0") );
    }
    
    private boolean existeLista(Utilizador ut, List<Utilizador> lu) {
	for(Utilizador u : lu) {
            if(u != null && u.getUser().equals(ut.getUser())) {
                return true;
            }
        }
        return false;
    }
    
    private Utilizador existe(String user, List<Utilizador> lu) {
        for(Utilizador u : lu) {
            if(u != null && u.getUser().equals(user)) {
                return u;
            }
        }
        return null;
    }
}
