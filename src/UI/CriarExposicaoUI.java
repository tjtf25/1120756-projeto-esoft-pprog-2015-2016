/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.CriarExposicaoController;
import Model.CentroExposicoes;
import Model.Exposicao;
import Model.Organizador;
import Model.Utilizador;
import utils.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class CriarExposicaoUI {
    
    private final CentroExposicoes m_centro_exposicoes;
    private final CriarExposicaoController m_controllerCE;

    public CriarExposicaoUI(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
        m_controllerCE = new CriarExposicaoController(m_centro_exposicoes);
    }
    
    public void run() {
        //cria Exposição
        novaExposicao();
        //preenche exposição
        Exposicao exposicao = introduzDadosExposicao();
        //valida exposição
        m_controllerCE.validaExposicao();
        
        // apresenta dados
        System.out.println(exposicao.toString());
        
        // solicita confirmação
        if (Utils.confirmacao("Confirma dados (s/n)?").equals("s")) {
            //seleciona organizadores
            escolheOrganizadores();
            
            if (m_controllerCE.registaExposicao()){
                System.out.println("A exposição foi registada: " + exposicao.toString());
            } else {
                System.out.println("A exposição não foi registada por problema interno!");
            }
        } else {
            System.out.println("A exposição não foi registada por decisão do utilizador!");
        }
    }
    
    private void novaExposicao() {
        m_controllerCE.novaExposicao();
    }
    
    private Exposicao introduzDadosExposicao() {
        System.out.println("Introdução de dados da exposição");
        
        String strTitulo = Utils.readLineFromConsole("Título: ");
        String strDescritivo = Utils.readLineFromConsole("Descritivo: ");
        Date strDataIni = Utils.readDateFromConsole("Data inicial (dd-mm-aaaa): ");
        Date strDataFim = Utils.readDateFromConsole("Data final (dd-mm-aaaa): ");
        String strLocal = Utils.readLineFromConsole("Local: ");
        
        return m_controllerCE.setDados(strTitulo, strDescritivo, strDataIni, strDataFim, strLocal);
    }
    
    private void escolheOrganizadores() {
        
        List<Utilizador> lu = m_controllerCE.getListaUtilizadores();
        
        // escolher os utilizadores
        List<Utilizador> luEscolhidos = new ArrayList<Utilizador>();
        String user;
        
        do {
            // apresentar os utilizadores
            System.out.println("-- Utilizadores --");
            for(Utilizador u : lu){
                if(!existeLista(u,luEscolhidos)) {
                    System.out.println(u.mostraSimples());
                }
            }
       
            user = Utils.readLineFromConsole("Escolha pelo user (0-terminar):");

            if(!user.equals("0")) {
                Utilizador u = existe(user, lu);
                if (u != null) {
                    
                    Organizador o = m_controllerCE.novoOrganizador(user);
                    
                    // solicita confirmação
                    if (Utils.confirmacao("Confirma o Organizador (s/n)?").equals("s")) {
                        m_controllerCE.registaOrganizador();
                        luEscolhidos.add(u);
                        System.out.println("Organizador adicionado com sucesso!");
                    } else {
                        System.out.println("O Organizador não foi registado por decisão do utilizador!");
                    }
                }
            }
        } while (!user.equals("0") );
    }
    
    private boolean existeLista(Utilizador ut, List<Utilizador> lu) {
	for(Utilizador u : lu) {
            if(u != null && u.getUser().equals(ut.getUser())) {
                return true;
            }
        }
        return false;
    }
    
    private Utilizador existe(String user, List<Utilizador> lu) {
        for(Utilizador u : lu) {
            if(u != null && u.getUser().equals(user)) {
                return u;
            }
        }
        return null;
    }
}
