/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.CriarPerfilUtilizadorController;
import Model.CentroExposicoes;
import Model.Utilizador;
import utils.Utils;

/**
 *
 * @author Tiago Faria
 */
public class CriarPerfilUtilizadorUI {
    
    private final CentroExposicoes m_centro_exposicoes;
    private final CriarPerfilUtilizadorController controller;

    public CriarPerfilUtilizadorUI(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
        controller = new CriarPerfilUtilizadorController(m_centro_exposicoes);
    }
    
    public void run() {
        controller.novoUtilizador();
        Utilizador u = introduzDadosUtilizador();
        controller.validaUtilizador();
        System.out.println(u.toString());
        
        if (Utils.confirmacao("Confirma dados (s/n)?").equals("s")) {
            if (controller.registaUtilizador()) {
                System.out.println("O utilizador foi registado: " + u.toString());
            } else {
                System.out.println("O utilizador não foi registado por problema interno!");
            }
        } else {
            System.out.println("O utilizador não foi registado por decisão do utilizador!");
        }
    }
    
    private Utilizador introduzDadosUtilizador() {
        System.out.println("Introdução de dados do utilizador");
        
        String nome = Utils.readLineFromConsole("Nome: ");
        String email = Utils.readLineFromConsole("Email: ");
        String user = Utils.readLineFromConsole("User: ");
        String password = Utils.readLineFromConsole("Password: ");
        
        return controller.setDados(nome, email, user, password);
    }
}
