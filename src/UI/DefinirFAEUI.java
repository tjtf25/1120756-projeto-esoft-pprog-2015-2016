/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.DefinirFAEController;
import Model.CentroExposicoes;
import Model.Exposicao;
import Model.FAE;
import Model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Tiago Faria
 */
public class DefinirFAEUI {
    
    private final CentroExposicoes m_centro_exposicoes;
    private final DefinirFAEController controller;
    
    public DefinirFAEUI(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
        controller = new DefinirFAEController(m_centro_exposicoes);
    }
    
    public void run() {
        String id = "";
        List<Exposicao> exposicoesOrganizador = controller.getExposicaoOrganizador(id);
        
        String exp;
        
        boolean opt = true;
        
        do {
            // apresentar as exposições
            System.out.println("-- Exposições --");
            for(Exposicao e : exposicoesOrganizador){
                System.out.println(e.mostraSimples());
            }
       
            exp = Utils.readLineFromConsole("Escolha pelo título (0-terminar):");

            if(!exp.equals("0")) {
                Exposicao e = existeExposicao(exp, exposicoesOrganizador);
                if (e != null) {
                    controller.selectExposicao(e);
                    opt = false;
                    selecionaFAE();
                    System.out.println("A exposição foi atualizada: " + e.toString());
                }
            }
        } while (opt && !exp.equals("0"));
    }
    
    private void selecionaFAE() {
        
        List<Utilizador> lu = controller.getListaUtilizadores();
        
        // escolher os utilizadores
        List<Utilizador> luEscolhidos = new ArrayList<Utilizador>();
        String user;
        
        do {
            // apresentar os utilizadores
            System.out.println("-- Utilizadores --");
            for(Utilizador u : lu){
                if(!existeLista(u,luEscolhidos)) {
                    System.out.println(u.mostraSimples());
                }
            }
       
            user = Utils.readLineFromConsole("Escolha pelo user (0-terminar):");

            if(!user.equals("0")) {
                Utilizador u = existeUtilizador(user, lu);
                if (u != null) {
                    
                    FAE f = controller.novoFAE(user);
                    
                    // solicita confirmação
                    if (Utils.confirmacao("Confirma o FAE (s/n)?").equals("s")) {
                        controller.registaFAE();
                        luEscolhidos.add(u);
                        System.out.println("FAE adicionado com sucesso!");
                    } else {
                        System.out.println("O FAE não foi registado por decisão do utilizador!");
                    }
                }
            }
        } while (!user.equals("0") );
    }
    
    private boolean existeLista(Utilizador ut, List<Utilizador> lu) {
	for(Utilizador u : lu) {
            if(u != null && u.getUser().equals(ut.getUser())) {
                return true;
            }
        }
        return false;
    }
    
    private Utilizador existeUtilizador(String user, List<Utilizador> lu) {
        for(Utilizador u : lu) {
            if(u != null && u.getUser().equals(user)) {
                return u;
            }
        }
        return null;
    }
    
    private Exposicao existeExposicao(String exp, List<Exposicao> lista) {
        for(Exposicao e : lista) {
            if(e != null && e.getTitulo().equals(exp)) {
                return e;
            }
        }
        return null;
    }
}
