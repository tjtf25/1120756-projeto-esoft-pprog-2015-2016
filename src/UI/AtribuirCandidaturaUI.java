/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AtribuirCandidaturaController;
import Model.Candidatura;
import Model.CentroExposicoes;
import Model.Exposicao;
import Model.FAE;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Tiago Faria
 */
public class AtribuirCandidaturaUI {
    
    private final CentroExposicoes m_centro_exposicoes;
    private final AtribuirCandidaturaController controller;

    public AtribuirCandidaturaUI(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
        controller = new AtribuirCandidaturaController(m_centro_exposicoes);
    }
    
    public void run() {
        String id = "";
        List<Exposicao> exposicoes = controller.getExposicaoOrganizador(id);
        
        String exp;
        
        boolean opt = true;
        
        do {
            // apresentar as exposições
            System.out.println("-- Exposições --");
            for(Exposicao e : exposicoes){
                System.out.println(e.mostraSimples());
            }
       
            exp = Utils.readLineFromConsole("Escolha pelo título (0-terminar):");

            if(!exp.equals("0")) {
                Exposicao e = existeExposicao(exp, exposicoes);
                if (e != null) {
                    controller.selectExposicao(e);
                    opt = false;
                    selecionaCandidatura();
                }
            }
        } while (opt && !exp.equals("0"));
    }
    
    private void selecionaCandidatura() {
        List<Candidatura> candidaturas = controller.getListaCandidaturas();
        
        String nome;
        
        boolean opt = true;
        
        do {
            // apresentar as candidaturas
            System.out.println("-- Candidaturas --");
            for(Candidatura c : candidaturas){
                System.out.println(c.mostraSimples());
            }
       
            nome = Utils.readLineFromConsole("Escolha pelo nome (0-terminar):");

            if(!nome.equals("0")) {
                Candidatura c = existeCandidatura(nome, candidaturas);
                if (c != null) {
                    controller.selectCandidatura(c);
                    
                    selecionaFAE(c);
                }
            }
        } while (!nome.equals("0"));
    }
    
    private void selecionaFAE(Candidatura c) {
        List<FAE> faes = controller.getListaFAE();
        
        String user;
        
        boolean opt = true;
        
        do {
            // apresentar os faes
            System.out.println("-- FAEs --");
            for(FAE f : faes){
                System.out.println(f.getUtilizador().mostraSimples());
            }
       
            user = Utils.readLineFromConsole("Escolha pelo user (0-terminar):");

            if(!user.equals("0")) {
                FAE f = existeFAE(user, faes);
                if (c != null) {
                    controller.selectFAE(f);
                    opt = false;
                    confirma(c, f);
                }
            }
        } while (opt && !user.equals("0"));
    }
    
    private void confirma(Candidatura c, FAE f) {
        System.out.println(c.toString());
        System.out.println(f.toString());
        
        if (Utils.confirmacao("Confirma dados (s/n)?").equals("s")) {
            controller.atribuirCandidatura();
            System.out.println("A candidatura foi atualizada com sucesso: " + c.toString());
        } else {
            System.out.println("A candidatura não foi atualizada por decisão do utilizador!");
        }
    }
    
    private Exposicao existeExposicao(String exp, List<Exposicao> lista) {
        for(Exposicao e : lista) {
            if(e != null && e.getTitulo().equals(exp)) {
                return e;
            }
        }
        return null;
    }
    
    private Candidatura existeCandidatura(String nome, List<Candidatura> lista) {
        for(Candidatura c : lista) {
            if(c != null && c.getEmpresa().equals(nome)) {
                return c;
            }
        }
        return null;
    }
    
    private FAE existeFAE(String user, List<FAE> lista) {
        for(FAE f : lista) {
            if(f != null && f.getUtilizador().getUser().equals(user)) {
                return f;
            }
        }
        return null;
    }
}
