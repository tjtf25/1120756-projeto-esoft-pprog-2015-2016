/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.CentroExposicoes;

/**
 *
 * @author Tiago Faria
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            CentroExposicoes centro_exposicoes = new CentroExposicoes();

            MenuUI uiMenu = new MenuUI(centro_exposicoes);

            uiMenu.run();
        }
        catch( Exception e ) {
            e.printStackTrace();
        }
    }
    
}
