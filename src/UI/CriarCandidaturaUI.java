/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.CriarCandidaturaController;
import Model.Candidatura;
import Model.CentroExposicoes;
import Model.Exposicao;
import Model.Produto;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Tiago Faria
 */
public class CriarCandidaturaUI {
    
    private final CentroExposicoes m_centro_exposicoes;
    private final CriarCandidaturaController controller;

    public CriarCandidaturaUI(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
        controller = new CriarCandidaturaController(m_centro_exposicoes);
    }
    
    public void run() {
        List<Exposicao> lista = controller.getListaExposicao();
        
        String exp;
        
        boolean opt = true;
        
        do {
            // apresentar as exposições
            System.out.println("-- Exposições --");
            for(Exposicao e : lista){
                System.out.println(e.mostraSimples());
            }
       
            exp = Utils.readLineFromConsole("Escolha pelo título (0-terminar):");

            if(!exp.equals("0")) {
                Exposicao e = existeExposicao(exp, lista);
                if (e != null) {
                    controller.selectExposicao(e);
                    opt = false;
                    novaCandidatura();
                    System.out.println("A exposição foi atualizada: " + e.toString());
                }
            }
        } while (opt && !exp.equals("0"));
    }
    
    private void novaCandidatura() {
        controller.novaCandidatura();
        
        Candidatura candidatura = introduzDadosCandidatura();
        
        controller.validaCandidatura();
        
        System.out.println(candidatura.toString());
        
        if (Utils.confirmacao("Confirma dados (s/n)?").equals("s")) {
            introduzirProdutos(candidatura);
        } else {
            System.out.println("A candidatura não foi registada por decisão do utilizador!");
        }
    }
    
    private void introduzirProdutos(Candidatura c) {
        String nome;
        do {
            System.out.println("Introdução de produto (0 - terminar)");
            nome = Utils.readLineFromConsole("Nome do produto: ");
            if (!nome.equals("0")) {
                Produto produto = controller.novoProduto(nome);
                
                System.out.println(produto.toString());
                
                if (Utils.confirmacao("Confirma dados (s/n)?").equals("s")) {
                    if (controller.registaProduto()) {
                        System.out.println("O produto foi registado: " + produto.toString());
                    } else {
                        System.out.println("O produto não foi registada por problema interno!");
                    }
                } else {
                    System.out.println("O produto não foi registado por decisão do utilizador!");
                }
            }
        } while(!nome.equals("0"));
        
        if (controller.registaCandidatura()) {
            System.out.println("A candidatura foi registada: " + c.toString());
        } else {
            System.out.println("A candidatura não foi registada por problema interno!");
        }
    }
    
    private Candidatura introduzDadosCandidatura() {
        System.out.println("Introdução de dados da candidatura");
        
        String empresa = Utils.readLineFromConsole("Nome da empresa: ");
        String morada = Utils.readLineFromConsole("Morada: ");
        String telemovel = Utils.readLineFromConsole("Telemovel: ");
        String areaExp = Utils.readLineFromConsole("Área do Expositor: ");
        String numeroConv = Utils.readLineFromConsole("Número de Convites: ");
        
        double areaExpositor = Double.parseDouble(areaExp);
        int numeroConvites = Integer.parseInt(numeroConv);
        
        return controller.setDados(empresa, morada, telemovel, areaExpositor, numeroConvites);
    }
    
    private Exposicao existeExposicao(String exp, List<Exposicao> lista) {
        for(Exposicao e : lista) {
            if(e != null && e.getTitulo().equals(exp)) {
                return e;
            }
        }
        return null;
    }
}
