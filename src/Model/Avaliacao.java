/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Tiago Faria
 */
public class Avaliacao {
    
    private boolean decisao;
    private String justificacao;
    
    public Avaliacao() {
        
    }
    
    public void setDecisao(boolean decisao) {
        this.decisao = decisao;
    }
    
    public void setJustificacao(String justificacao) {
        this.justificacao = justificacao;
    }
    
    public boolean valida() {
        return true;
    }
    
    public String toString() {
        String str = "Decisão:\n";
        str += "\tDecisão: " + this.decisao + "\n";
        str += "\tJustificação: " + this.justificacao + "\n";
        
        return str;
    }
}
