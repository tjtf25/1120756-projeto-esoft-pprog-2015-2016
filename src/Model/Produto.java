/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Tiago Faria
 */
public class Produto {
    
    private String nome;
    
    public Produto() {
        
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getNome() {
        return nome;
    }
    
    public boolean valida() {
        return true;
    }
    
    public String toString() {
        String str = "Produto:\n";
        str += "\tNome do Produto: " + this.nome + "\n";
        
        return str;
    }
}
