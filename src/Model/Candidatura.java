/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class Candidatura {
    
    private Expositor expositor;
    private double areaExpositor;
    private int numeroConvites;
    private List<Produto> listaProdutos;
    private FAE fae;
    private Avaliacao avaliacao;
    
    public Candidatura() {
        expositor = new Expositor();
        listaProdutos = new ArrayList<Produto>();
    }
    
    public void setEmpresa(String empresa) {
        expositor.setNome(empresa);
    }
    
    public String getEmpresa() {
        return expositor.getNome();
    }
    
    public void setMorada(String morada) {
        expositor.setMorada(morada);
    }
    
    public void setTelemovel(String telemovel) {
        expositor.setTelemovel(telemovel);
    }
    
    public void setAreaExpositor(double areaExpositor) {
        this.areaExpositor = areaExpositor;
    }
    
    public void setNumeroConvites(int numeroConvites) {
        this.numeroConvites = numeroConvites;
    }
    
    public void setFAE(FAE f) {
        fae = f;
    }
    
    public void setAvaliacao(Avaliacao d) {
        avaliacao = d;
    }
    
    public boolean valida() {
        return true && expositor.valida();
    }
    
    public Produto novoProduto() {
        return new Produto();
    }
    
    public boolean validaProduto(Produto p) {
        return p.valida() && validaNovoProduto(p);
    }
    
    public boolean validaNovoProduto(Produto p) {
        return true;
    }
    
    public boolean registaProduto(Produto p) {
        return addProduto(p);
    }
    
    private boolean addProduto(Produto p) {
        return listaProdutos.add(p);
    }
    
    public Avaliacao novaAvaliacao() {
        return new Avaliacao();
    }
    
    public boolean validaAvaliacao(Avaliacao d) {
        return d.valida() && validaNovaAvaliacao(d);
    }
    
    private boolean validaNovaAvaliacao(Avaliacao d) {
        return true;
    }
    
    public void registaAvaliacao(Avaliacao d) {
        setAvaliacao(d);
    }
    
    public String toString() {
        String str = "Candidatura:\n";
        str += "\tNome da Empresa: " + this.expositor.getNome() + "\n";
        str += "\tMorada: " + this.expositor.getMorada() + "\n";
        str += "\tTelemovel: " + this.expositor.getTelemovel() + "\n";
        str += "\tÁrea do Expositor: " + this.areaExpositor + "\n";
        str += "\tNúmero de Convites: " + this.numeroConvites + "\n";
        str += "\tProdutos:\n";
        for (Produto p : listaProdutos) {
            str += "\t\t" + p.getNome() + "\n";
        }
        if (fae != null) {
            str += "\tFAE: " + this.fae.getUtilizador().getUser() + "\n";
        }
        
        return str;
    }
    
    public String mostraSimples() {
        return expositor.getNome();
    }
}
