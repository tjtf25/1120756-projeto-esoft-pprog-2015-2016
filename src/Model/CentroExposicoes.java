/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class CentroExposicoes {
    
    private final List<Utilizador> m_listaUtilizadores;
    private final List<Exposicao> m_listaExposicoes;
    
    
    public CentroExposicoes() {
        m_listaUtilizadores = new ArrayList<Utilizador>();
        m_listaExposicoes = new ArrayList<Exposicao>();
        
        fillInData(); // preencher com dados para teste.
    }
    
    void fillInData() {
        
        Utilizador u = new Utilizador();
        u.setNome("João");u.setEmail("joao@esoft.pt");u.setUser("joao");u.setPwd("abc");
        m_listaUtilizadores.add(u);
        
        Utilizador u2 = new Utilizador();
        u2.setNome("ricardo");u2.setEmail("ricardo@esoft.pt");u2.setUser("ricardo");u2.setPwd("def");
        m_listaUtilizadores.add(u2);
        
        Utilizador u3 = new Utilizador();
        u3.setNome("pedro");u3.setEmail("pedro@esoft.pt");u3.setUser("pedro");u3.setPwd("ghi");
        m_listaUtilizadores.add(u3);
        
        Utilizador u4 = new Utilizador();
        u4.setNome("joaquim");u4.setEmail("joaquim@esoft.pt");u4.setUser("joaquim");u4.setPwd("jkl");
        m_listaUtilizadores.add(u4);
        
        Utilizador u5 = new Utilizador();
        u5.setNome("Bruno");u5.setEmail("bruno@esoft.pt");u5.setUser("bruno");u5.setPwd("mno");
        m_listaUtilizadores.add(u5);
    }
    
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }
    
    public boolean validaUtilizador(Utilizador u) {
        return u.valida() && validaNovoUtilizador(u);
    }
    
    private boolean validaNovoUtilizador(Utilizador u) {
        for (Utilizador ut : m_listaUtilizadores) {
            if (ut.getUser().equals(u.getUser()) || ut.getEmail().equals(u.getEmail())) {
                return false;
            }
        }
        return true;
    }
    
    public boolean registaUtilizador(Utilizador u) {
        return addUtilizador(u);
    }
    
    private boolean addUtilizador(Utilizador u) {
        return m_listaUtilizadores.add(u);
    }
            
    public Exposicao novaExposicao() {
        return new Exposicao();
    }
    
    public boolean registaExposicao(Exposicao e) {
        return addExposicao(e);
    }
    
    private boolean addExposicao(Exposicao e) {
        return m_listaExposicoes.add(e);
    }
    
    public boolean validaExposicao(Exposicao e) {
        return e.valida() && validaNovaExposicao(e);
    }
    
    private boolean validaNovaExposicao(Exposicao e) {
        return true;
    }
    
    public List<Utilizador> getUtilizadores() {
        return m_listaUtilizadores;
    }
    
    public Utilizador getUtilizador(String id) {
        for (Utilizador u : m_listaUtilizadores) {
            if (u.getUser().equals(id)) {
                return u;
            }
        }
        return null;
    }
    
    public List<Exposicao> getExposicaoOrganizador(String id) {
        List<Exposicao> lista = new ArrayList<>();
        for (Exposicao e : m_listaExposicoes) {
            for (Organizador o : e.getListaOrganizadores()) {
                if (o.getUtilizador().getUser().equals(id)) {
                    lista.add(e);
                }
            }
        }
        return lista;
    }
    
    public List<Utilizador> getUtilizadoresNaoConfirmados() {
        List<Utilizador> lista = new ArrayList<Utilizador>();
        for (Utilizador u : this.m_listaUtilizadores) {
            if (!u.getConfirmacao()) {
                lista.add(u);
            }
        }
        return lista;
    }
    
    public List<Exposicao> getListaExposicao() {
        return m_listaExposicoes;
    }
    
    public List<Exposicao> getExposicaoFAE(String id) {
        List<Exposicao> lista = new ArrayList<>();
        for (Exposicao e : m_listaExposicoes) {
            for (FAE f : e.getListaFAE()) {
                if (f.getUtilizador().getUser().equals(id)) {
                    lista.add(e);
                }
            }
        }
        return lista;
    }
}
