/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class Exposicao {
    
    private String m_strTitulo;
    private String m_strDescritivo;
    private Date m_dtDtIni;
    private Date m_dtDtFim;
    private String m_strLocal;
    private final List<Organizador> m_listaOrganizadores;
    private final List<FAE> m_listaFAE;
    private final List<Candidatura> m_listaCandidaturas;

    public Exposicao() {
        m_listaOrganizadores = new ArrayList<Organizador>();
        m_listaFAE = new ArrayList<FAE>();
        m_listaCandidaturas = new ArrayList<Candidatura>();
    }
 
    public boolean valida() {
        System.out.println("Exposição: valida: " + this.toString());
        return true;
    }
    
    @Override
    public String toString() {
        String str = "Exposição:\n";
        str += "\tTitulo: " + this.m_strTitulo + "\n";
        str += "\tTexto descritivo: " + this.m_strDescritivo + "\n";
        str += "\tData inicial: " + utils.Utils.convertDateToString(this.m_dtDtIni) + "\n";
        str += "\tData final: " + utils.Utils.convertDateToString(this.m_dtDtFim) + "\n";
        str += "\tLocal: " + this.m_strLocal + "\n";
        str += "\tOrganizadores:\n";
        for (Organizador o : m_listaOrganizadores) {
            str += "\t\t" + o.getUtilizador().getUser() + "\n";
        }
        str += "\tFAE:\n";
        for (FAE f : m_listaFAE) {
            str += "\t\t" + f.getUtilizador().getUser() + "\n";
        }
        str += "\tCandidaturas:\n";
        for (Candidatura c : m_listaCandidaturas) {
            str += "\t\t" + c.getEmpresa() + "\n";
        }
        return str;
    }
    
    public String mostraSimples(){
        return m_strTitulo+"-"+m_strDescritivo;
    }
   
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }
    
    public String getTitulo() {
        return m_strTitulo;
    }

    public void setDescritivo(String strDescritivo) {
        this.m_strDescritivo = strDescritivo;
    }

    public void setPeriodo(Date dtDtIni, Date dtDtFim) {
        this.m_dtDtIni = dtDtIni;
        this.m_dtDtFim = dtDtFim;
    }

    public void setLocal(String strLocal) {
        this.m_strLocal = strLocal;
    }

    private boolean validaOrganizador(Organizador o) {
        System.out.println("Exposição: validaOrganizador: " + o.toString());
        return true;
    }
     
    public Organizador novoOrganizador(Utilizador u) {
        Organizador o = new Organizador();
        o.setUtilizador(u);
        if (o.valida() && validaOrganizador(o)){
            return o;
        }
        return null;
    }
    
    public void registaOrganizador(Organizador o) {
        addOrganizador(o);
    }
    
    private void addOrganizador(Organizador o) {
        m_listaOrganizadores.add(o);
    }
    
    public List<Organizador> getListaOrganizadores() {
        return m_listaOrganizadores;
    }
    
    public FAE novoFAE(Utilizador u) {
        FAE f = new FAE();
        f.setUtilizador(u);
        if (f.valida() && validaFAE(f)){
            return f;
        }
        return null;
    }
    
    private boolean validaFAE(FAE f) {
        System.out.println("Exposição: validaFAE: " + f.toString());
        return true;
    }
    
    public void registaFAE(FAE f) {
        addFAE(f);
    }
    
    private void addFAE(FAE f) {
        m_listaFAE.add(f);
    }
    
    public List<FAE> getListaFAE() {
        return m_listaFAE;
    }
    
    public Candidatura novaCandidatura() {
        return new Candidatura();
    }
    
    public boolean validaCandidatura(Candidatura c) {
        return c.valida() && validaNovaCandidatura(c);
    }
    
    private boolean validaNovaCandidatura(Candidatura c) {
        return true;
    }
    
    public boolean registaCandidatura(Candidatura c) {
        return addCandidatura(c);
    }
    
    private boolean addCandidatura(Candidatura c) {
        return m_listaCandidaturas.add(c);
    }
    
    public List<Candidatura> getListaCandidaturas() {
        return m_listaCandidaturas;
    }
    
    public List<Candidatura> getCandidaturaFAE(String id) {
        return m_listaCandidaturas;
    }
}
