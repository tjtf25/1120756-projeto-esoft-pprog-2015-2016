/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Tiago Faria
 */
public class Organizador {
    
    private Utilizador m_Utilizador;
    
    public Organizador() {
        
    }
    
    public void setUtilizador(Utilizador u) {
        this.m_Utilizador = u;
    }
 
    public Utilizador getUtilizador() {
        return m_Utilizador;
    }
    
    public boolean valida() {
        return true;
    }
 
    @Override
    public String toString() {
        String str = "Organizador:\n";
        str += "\tUser: " + this.m_Utilizador.getUser() + "\n";

        return str;
    }
}
