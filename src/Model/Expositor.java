/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Tiago Faria
 */
public class Expositor {
    
    private String nome;
    private String morada;
    private String telemovel;
    
    public Expositor() {
        
    }
    
    public void setNome(String empresa) {
        this.nome = empresa;
    }
    
    public String getNome() {
        return nome;
    }
    
    public void setMorada(String morada) {
        this.morada = morada;
    }
    
    public String getMorada() {
        return morada;
    }
    
    public void setTelemovel(String telemovel) {
        this.telemovel = telemovel;
    }
    
    public String getTelemovel() {
        return telemovel;
    }
    
    public boolean valida() {
        return true;
    }
    
    public String toString() {
        String str = "Expositor:\n";
        str += "\tNome da Empresa: " + this.nome + "\n";
        str += "\tMorada: " + this.morada + "\n";
        str += "\tTelemovel: " + this.telemovel + "\n";
        
        return str;
    }
}
