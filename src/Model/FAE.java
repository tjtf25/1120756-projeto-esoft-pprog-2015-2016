/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Tiago Faria
 */
public class FAE {
    
    private Utilizador utilizador;
    
    public FAE() {
        
    }
    
    public void setUtilizador(Utilizador u) {
        this.utilizador = u;
    }
 
    public Utilizador getUtilizador() {
        return utilizador;
    }
    
    public boolean valida() {
        return true;
    }
    
    @Override
    public String toString() {
        String str = "FAE:\n";
        str += "\tUser: " + this.utilizador.getUser() + "\n";

        return str;
    }
}
