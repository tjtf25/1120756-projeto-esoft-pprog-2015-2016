/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CentroExposicoes;
import Model.Exposicao;
import Model.Produto;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class CriarCandidaturaController {
    
    private final CentroExposicoes m_centro_exposicoes;
    private Exposicao exposicao;
    private Candidatura candidatura;
    private Produto produto;

    public CriarCandidaturaController(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
    }
    
    public List<Exposicao> getListaExposicao() {
        return m_centro_exposicoes.getListaExposicao();
    }
    
    public void selectExposicao(Exposicao e) {
        exposicao = e;
    }
    
    public void novaCandidatura() {
        candidatura = exposicao.novaCandidatura();
    }
    
    public Candidatura setDados(String empresa, String morada, String telemovel, double areaExpositor, int numeroConvites) {
        candidatura.setEmpresa(empresa);
        candidatura.setMorada(morada);
        candidatura.setTelemovel(telemovel);
        candidatura.setAreaExpositor(areaExpositor);
        candidatura.setNumeroConvites(numeroConvites);
        return candidatura;
    }
    
    public void validaCandidatura() {
        exposicao.validaCandidatura(candidatura);
    }
    
    public Produto novoProduto(String nome) {
        produto = candidatura.novoProduto();
        produto.setNome(nome);
        candidatura.validaProduto(produto);
        return produto;
    }
    
    public boolean registaProduto() {
        return candidatura.registaProduto(produto);
    }
    
    public boolean registaCandidatura() {
        return exposicao.registaCandidatura(candidatura);
    }
}
