/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicoes;
import Model.Exposicao;
import Model.FAE;
import Model.Utilizador;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class DefinirFAEController {
    
    private final CentroExposicoes m_centro_exposicoes;
    private Exposicao exposicao;
    private FAE fae;
    
    public DefinirFAEController(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
    }
    
    public List<Exposicao> getExposicaoOrganizador(String id) {
        return m_centro_exposicoes.getExposicaoOrganizador(id);
    }
    
    public void selectExposicao(Exposicao e) {
        exposicao = e;
    }
    
    public List<Utilizador> getListaUtilizadores() {
        return m_centro_exposicoes.getUtilizadores();
    }
    
    public FAE novoFAE(String id) {
        Utilizador u = m_centro_exposicoes.getUtilizador(id);
        fae = exposicao.novoFAE(u);
        return fae;
    }
    
    public void registaFAE() {
        exposicao.registaFAE(fae);
    }
}
