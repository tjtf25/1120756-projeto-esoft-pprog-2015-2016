/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicoes;
import Model.Exposicao;
import Model.Organizador;
import Model.Utilizador;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class CriarExposicaoController {
    
    private final CentroExposicoes m_centro_exposicoes;
    private Exposicao m_exposicao;
    private Organizador m_organizador;

    public CriarExposicaoController(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
    }
    
     public void novaExposicao() {
        m_exposicao = m_centro_exposicoes.novaExposicao();
    }
 
    public List<Utilizador> getListaUtilizadores() {
        return m_centro_exposicoes.getUtilizadores();
    }
        
    public Exposicao setDados(String strTitulo, String strDescritivo, Date strDataIni, Date strDataFim, String strLocal) {
        m_exposicao.setTitulo(strTitulo);
        m_exposicao.setDescritivo(strDescritivo);
        m_exposicao.setPeriodo(strDataIni, strDataFim);
        m_exposicao.setLocal(strLocal);
        return m_exposicao;
    }
    
    public boolean validaExposicao() {
        return m_centro_exposicoes.validaExposicao(m_exposicao);
    }
    
    public Organizador novoOrganizador(String id) {
        Utilizador u = m_centro_exposicoes.getUtilizador(id);
        m_organizador = m_exposicao.novoOrganizador(u);
        return m_organizador;
    }
    
    public void registaOrganizador() {
        m_exposicao.registaOrganizador(m_organizador);
    }
    
    public boolean registaExposicao() {
        return m_centro_exposicoes.registaExposicao(m_exposicao);
    }
}
