/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicoes;
import Model.Utilizador;

/**
 *
 * @author Tiago Faria
 */
public class CriarPerfilUtilizadorController {
    
    private final CentroExposicoes m_centro_exposicoes;
    private Utilizador utilizador;

    public CriarPerfilUtilizadorController(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
    }
    
    public void novoUtilizador() {
        utilizador = m_centro_exposicoes.novoUtilizador();
    }
    
    public Utilizador setDados(String nome, String email, String user, String password) {
        utilizador.setNome(nome);
        utilizador.setEmail(email);
        utilizador.setUser(user);
        utilizador.setPwd(password);
        return utilizador;
    }
    
    public boolean validaUtilizador() {
        return m_centro_exposicoes.validaUtilizador(utilizador);
    }
    
    public boolean registaUtilizador() {
        return m_centro_exposicoes.registaUtilizador(utilizador);
    }
}
