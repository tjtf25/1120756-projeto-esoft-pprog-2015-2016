/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CentroExposicoes;
import Model.Avaliacao;
import Model.Exposicao;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class AvaliarCandidaturaController {
    
    private final CentroExposicoes m_centro_exposicoes;
    private Exposicao exposicao;
    private Candidatura candidatura;
    private Avaliacao avaliacao;
    
    public AvaliarCandidaturaController(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
    }
    
    public List<Exposicao> getExposicaoFAE(String id) {
        return m_centro_exposicoes.getExposicaoFAE(id);
    }
    
    public void selectExposicao(Exposicao e) {
        exposicao = e;
    }
    
    public List<Candidatura> getCandidaturaFAE(String id) {
        return exposicao.getCandidaturaFAE(id);
    }
    
    public void selectCandidatura(Candidatura c) {
        candidatura = c;
    }
    
    public void novaAvaliacao() {
        avaliacao = candidatura.novaAvaliacao();
    }
    
    public Avaliacao setDados(boolean decisao, String justificacao) {
        this.avaliacao.setDecisao(decisao);
        this.avaliacao.setJustificacao(justificacao);
        return this.avaliacao;
    }
    
    public boolean validaAvaliacao() {
        return candidatura.validaAvaliacao(avaliacao);
    }
    
    public void registaAvaliacao() {
        candidatura.registaAvaliacao(avaliacao);
    }
}
