/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Candidatura;
import Model.CentroExposicoes;
import Model.Exposicao;
import Model.FAE;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class AtribuirCandidaturaController {
    
    private final CentroExposicoes m_centro_exposicoes;
    private Exposicao exposicao;
    private Candidatura candidatura;
    private FAE fae;

    public AtribuirCandidaturaController(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
    }
    
    public List<Exposicao> getExposicaoOrganizador(String id) {
        return m_centro_exposicoes.getExposicaoOrganizador(id);
    }
    
    public void selectExposicao(Exposicao e) {
        exposicao = e;
    }
    
    public List<Candidatura> getListaCandidaturas() {
        return exposicao.getListaCandidaturas();
    }
    
    public void selectCandidatura(Candidatura c) {
        candidatura = c;
    }
    
    public List<FAE> getListaFAE() {
        return exposicao.getListaFAE();
    }
    
    public void selectFAE(FAE f) {
        fae = f;
    }
    
    public void atribuirCandidatura() {
        candidatura.setFAE(fae);
    }
}
