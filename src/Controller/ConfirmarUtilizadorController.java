/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroExposicoes;
import Model.Utilizador;
import java.util.List;

/**
 *
 * @author Tiago Faria
 */
public class ConfirmarUtilizadorController {
    
    private final CentroExposicoes m_centro_exposicoes;
    private Utilizador utilizador;

    public ConfirmarUtilizadorController(CentroExposicoes centro_exposicoes) {
        m_centro_exposicoes = centro_exposicoes;
    }
    
    public List<Utilizador> getUtilizadoresNaoConfirmados() {
        return m_centro_exposicoes.getUtilizadoresNaoConfirmados();
    }
    
    public void selectUtilizador(Utilizador u) {
        utilizador = u;
    }
    
    public void confirmarUtilizador() {
        utilizador.confirma();
    }
}
